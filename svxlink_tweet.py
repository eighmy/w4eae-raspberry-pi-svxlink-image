#!/usr/bin/env python2.7
# svxlink_tweet.py
# Author: Gene Eighmy W4EAE
# Creation Date:  12 June 2015
#
# Description: A helper app for svxlink that monitors the svxlog and
# Tweets Connection events.  When a station connects, a message like
# "12 Jun 2015 20:42:11: W4EAE: EchoLink QSO state changed to CONNECTED"
# is tweeted.  You can set up notification on your smart phone or regular
# cell phone and be alterted whenever a station connects to your echolink
# node.
#
# This app requires Tweepy which can be installed by following these steps 
# at the command prompt:
#  
# 1. Update 
#      sudo apt-get update
# 2. Install the latest python-dev and python-pip. 
#      sudo apt-get install python-dev python-pip
# 3. Uninstall/reinstall pip
#      sudo pip install -U pip
# 4. Install tweepy
#      sudo pip install tweepy
#
# Setup an Application on your twitter account.  You may want to setup a separate
# account for the Raspberry Pi to keep the chatter off your personal twitter account.
#
# 1. Login to http://apps.twitter.com with your login credentials
# 2. Click on "Create New App" button
# 3. Enter the name of your App, e.g., "My Raspi SVXLINK App", Description and a website URL or
#    placeholder.
# 4. Click the Yes checkbox and "Create Your Twitter Applicaiton" button.
# 5. The next screen will have a menu at the top with Details, Settings, Keys and Access Tokens
#    and Permissions.  Click on Keys and Access Tokens.
# 6. Click on the "Generate Consumer Key and Secret" button if needed and the 
#    "Generate My Access Token and Token Secret" button.
# 7. Copy and paste the values for the consumer key, consumer secret, access token and access
#    token secret into the code below.
#
# NOTE: The default svxlog is located in /var/log/svxlog.  If you have moved it, change the filename
# variable in the code to match.
#
# Running svxlink_tweet
# 
# Copy svxlink_tweet.py into your home directory for testing.  Once you have it working the way you
# like, you can move it to /usr/local/bin to access it without adding a path.
# 
# Enable execute privilege:
#      chmod +x svxlink_tweet.py
#
# to run from your home directory:  
#      ./svxlink_tweet.py
#
# The application will display changes to the svxlog to the console unless you add a -quiet argument.
#
# to run in the background:
#      ./svxlink_tweet.py &
#
# Connect to your svxlink node and confirm that the tweet was sent.  Once you have it working the way
# you like, copy it to the /usr/local/bin directory which allows you to run it globally without entering
# the "./" in front.
#
#      sudo cp svxlink_tweet.py /usr/local/bin/svxlink_tweet
#
# To run just type "svxlink_tweet" or "svxlink_tweet &" at the command prompt.
#
# You can also use -q or --quiet to filter out the non-connect messages.  This will display only the 
# the connect messages that are tweeted.  You may also use this when running in the background to save 
# some CPU time.
#
# svxlink_tweet - A tweet utility for svxlink
# Copyright (C) 2015 Gene Eighmy / W4EAE
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import time
import tweepy  
import sys  
  
# Consumer keys and access tokens, used for OAuth  
consumer_key        = 'YOUR_CONSUMER_KEY_HERE'  
consumer_secret     = 'YOUR_CONSUMER_SECRET_HERE'  
access_token        = 'YOUR_ACCESS_TOKEN_HERE'  
access_token_secret = 'YOUR_ACCESS_TOKEN_SECRET_HERE'  
  
filename = "/var/log/svxlog"


def tweet(aString, tApi):
    if len(aString) >= 2:  
        tweet_text = aString  
    else:  
        tweet_text = "Raspberry Pi svxlink tweet bot is sad :-("  
  
    if len(tweet_text) <= 140:  
        tApi.update_status(status=tweet_text)  
    else:  
        print "Tweet too long. 140 chars Max." 

def follow(thefile, tApi, beVerbose):
    thefile.seek(0,2) # Go to the end of the file
    while True:
        line = thefile.readline()
        if not line:
            time.sleep(0.1) # Sleep briefly
            continue
        else:
            nline = line.rstrip("\r\n") 
            
            if("EchoLink QSO state changed to CONNECTED" in nline):
                print "Tweet:%s" % nline
                tweet(nline, tApi)
            else:
                if(beVerbose):
                    print nline


verbose = True
show_help = False

try:

    # OAuth process, using the keys and tokens  
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)  
    auth.set_access_token(access_token, access_token_secret)  
   
    # Creation of the actual interface, using authentication  
    api = tweepy.API(auth)  

    if len(sys.argv) >= 2:  
        arg_text = sys.argv[1]
        show_help = True
        if(arg_text == "--quiet"):
            print "Quiet Mode"
            verbose = False
            show_help = False
        elif(arg_text == "-q"):
            print "Quiet Mode"
            verbose = False
            show_help = False
        else:
            show_help = True

    if(not show_help):
        print "Monitoring svxlog for CONNECTED %s" % filename
        print "CTRL-C to quit..."
        logfile = open(filename, "r")
        follow(logfile, api, verbose)
    else:
        print "usage: svxlink_tweet.py [-h] [-q]"
        print "    to run as a background task: svxlink_tweet.py [-q] &"
        print "\noptional arguments:"
        print "    -q, --quiet           suppress log output and show tweeted messages only "
        print "    -h, --help            show this help message and exit"

except (KeyboardInterrupt, SystemExit):  
    # here you put any code you want to run before the program   
    # exits when you press CTRL+C  
    print "User aborted..."

 
  
#except:  
    # this catches ALL other exceptions including errors.  
    # You won't get any error messages for debugging  
    # so only use it once your code is working  
#    print "ERROR: Unknown error or exception occurred!"  
#    Flog("UNKNOWN_ERROR")

finally:  
    print "Exiting..."
